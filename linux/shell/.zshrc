#!/usr/bin/zsh

export LANG=en_US.UTF-8
export MANPATH="/usr/local/man:$MANPATH"
export ZSH=$HOME/.oh-my-zsh

export ANDROID_HOME=$HOME/Android/Sdk
export PATH=$PATH:$ANDROID_HOME/cmdline-tools/latest/bin
export QT_QPA_PLATFORM=xcb


COMPLETION_WAITING_DOTS="true"
DISABLE_AUTO_TITLE="true"
ENABLE_CORRECTION="false"
SPACESHIP_PROMPT_ASYNC=false
TERM=xterm-256color

zstyle ':omz:update' mode disabled
plugins=(
    asdf
    git
    kubectx
    zsh-autocomplete
    zsh-autosuggestions
    zsh-completions
    zsh-syntax-highlighting
)
autoload -U compinit && compinit
source $ZSH/oh-my-zsh.sh
eval "$(starship init zsh)"

source $HOME/.zsh_envs 2>/dev/null || true
source $HOME/.zsh_secrets 2>/dev/null || true
(cat $HOME/.path.list 2>/dev/null || true) |
    sed 's|\s*#\s*.*||g' |
    sed '/^[[:space:]]*$/d' |
    sed 's|\(.*\)|export PATH="\1:$PATH"|g' > \
        $HOME/.path.sh
source $HOME/.path.sh
source $HOME/.zsh_sources 2>/dev/null || true
source $HOME/.zsh_aliases 2>/dev/null || true
source $HOME/.zsh_functions 2>/dev/null || true
source $HOME/.zsh_secrets 2>/dev/null || true

autoload -U compinit && compinit

hour=$(date +"%H")

# Determine the greeting based on the current hour
if [ $hour -ge 0 -a $hour -lt 12 ]
then
    greeting="Good morning"
elif [ $hour -ge 12 -a $hour -lt 16 ]
then
    greeting="Good afternoon"
else
    greeting="Good evening"
fi

# fortune | cowsay | lolcat
echo " _________________________
< 🦇 FlyingFox Labs OS 🦇 >
 -------------------------
                    \\
              =/\    \\            /\\=
              / \\'._   (\_/)   _.'/ \\
             / .''._'--(o.o)--'_.''. \\
            /.' _/ |\`'=/ \" \\='\`| \\_ \`.\\
           /\` .' \`\\;-,'\\___/',-;/\` '. '\\
          /.-' jgs   \`\\(-V-)/\`       \`-.\\
          \`            \"   \"            \`
          $greeting Dharmendra Let's code🔥
" | lolcat

# bun completions

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
export JAVA_HOME=$(asdf where java)
export PATH="$JAVA_HOME/bin:$PATH"
